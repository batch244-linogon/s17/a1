/*var myName = window.prompt("Enter your first friend's name:");
console.log("Hello",myName);

var myName = window.prompt("Enter your second friend's name:");
console.log("Hello",myName);

var myName = window.prompt("Enter your third friend's name:");
console.log("Hello",myName);*/

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	
	let printDetails = function(){
	let fullName = prompt("what is your name?"); 
	let age = prompt("How old are you?"); 
	let location = prompt("Where do you live?");
	alert("Thank you for your input!");

	console.log('Hello, ' + fullName + '!');
	console.log('You are ' + age + ' years old.');
	console.log('You live in ' + location);
};
printDetails();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function faveBand(){
		console.log('1 Callalily');
		console.log('2 Cueshe');
		console.log('3 Eraserheads');
		console.log('4 BackStreet Boys');
		console.log('5 West Life');
	};

	faveBand();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function faveMovies(){
		console.log('1 Harry Potter');
		console.log('Rotten Tomatoes Rating: 80%');
		console.log('2 Lord of the Rings');
		console.log('Rotten Tomatoes Rating: 75%');
		console.log('3 Men in Black');
		console.log('Rotten Tomatoes Rating: 99%');
		console.log('4 x-men');
		console.log('Rotten Tomatoes Rating: 97%');
		console.log('5 Thor');
		console.log('Rotten Tomatoes Rating: 86%');
	};

	faveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();